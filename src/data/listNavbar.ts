

export default  [
   {
      id: "1",
      link: "/",
      title: "Home"
   },
   {
      id: "2",
      link: "/about",
      title: "About BOOST"
   },
   {
      id: "3",
      link: "/partners",
      title: "Partners"
   },
   {
      id: "4",
      link: "/events/categories",
      title: "Events"
   }
]